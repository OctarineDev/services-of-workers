@extends('includes.header')

@section('content')
    <section id="content">
        <div class="wr clear">
            <div class="main">
                <div class="title">
                    <h3>Регистрация</h3>
                </div>
                <div class="attention"><span>Обратите внимание!<br></span>При заполнении необходимо указывать только достоверные данные.<br>Фамилию, имя и дату рождения после сохранения самостоятельно изменить нельзя.</div>
                <form role="form" method="POST" action="{{ route('register') }}" class="registration form-horizontal">
                    {{ csrf_field() }}

                    <div class="form-block">
                        <div class="form-left">
                            <div class="circle">1</div>
                            <div class="gray-vertical">
                                <div class="vertical"></div>
                            </div>
                        </div>
                        <div class="form-right">
                            <div class="form-subtitle">Вид деятельности</div>
                            <div class="block field-of-activity">
                                <div class="about dib">Зарегистрироваться как</div>
                                <input checked class="radio" id="master" type="radio" name="role" value="master">
                                <label for="master">Мастер</label>
                                <input class="radio" id="customer" type="radio" name="role" value="customer">
                                <label for="customer">Заказчик</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-block">
                        <div class="form-left">
                            <div class="circle">2</div>
                            <div class="gray-vertical">
                                <div class="vertical"></div>
                            </div>
                        </div>
                        <div class="form-right">
                            <div class="form-subtitle">Персональная информация</div>
                            <div class="block personal-info-block">
                                <div class="about">Имя:</div>
                                <input name="name" value="{{ old('name') }}" required autofocus type="text">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                <div class="about">Фамилия:</div>
                                <input name="surname" value="{{ old('surname') }}" required autofocus type="text">
                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                                <div class="about">Дата рождения:</div>
                                <select name="birth_day" reuqired id="select1">
                                    <option disabled value="">день</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                </select>
                                <select name="birth_month" reuqired id="select2">
                                    <option disabled value="">месяц</option>
                                    <option value="Январь">Январь</option>
                                    <option value="Февраль">Февраль</option>
                                    <option value="Март">Март</option>
                                    <option value="Апрель">Апрель</option>
                                    <option value="Май">Май</option>
                                    <option value="Июнь">Июнь</option>
                                    <option value="Июль">Июль</option>
                                    <option value="Август">Август</option>
                                    <option value="Сентябрь">Сентябрь</option>
                                    <option value="Октябрь">Октябрь</option>
                                    <option value="Ноябрь">Ноябрь</option>
                                    <option value="Декабрь">Декабрь</option>
                                </select>
                                <select name="birth_year" reuqired id="select3">
                                    <option disabled value="">год</option>
                                    <option value="1981">1981</option>
                                    <option value="1982">1982</option>
                                    <option value="1983">1983</option>
                                    <option value="1984">1984</option>
                                    <option value="1985">1985</option>
                                    <option value="1986">1986</option>
                                    <option value="1987">1987</option>
                                    <option value="1988">1988</option>
                                    <option value="1989">1989</option>
                                </select>
                                <div class="clear"></div>
                                <div class="about dib">Пол</div>
                                <input checked class="radio" id="male" type="radio" name="gender" value="male">
                                <label for="male">Мужской</label>
                                <input class="radio" id="female" type="radio" name="gender" value="female">
                                <label for="female">Женский</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-block">
                        <div class="form-left">
                            <div class="circle">3</div>
                            <div class="gray-vertical">
                                <div class="vertical"></div>
                            </div>
                        </div>
                        <div class="form-right">
                            <div class="form-subtitle">Контактная информация</div>
                            <div class="block contact-info-block">
                                <div class="about">Город:</div>
                                <select reuqired name="city" id="select5">
                                    <option disabled value="">город</option>
                                    <option value="Москва">Москва</option>
                                    <option value="Питер">Питер</option>
                                    <option value="Белгород">Белгород</option>
                                    <option value="Уфа">Уфа</option>
                                    <option value="Астрахань">Астрахань</option>
                                    <option value="Чебоксары">Чебоксары</option>

                                </select>
                                <div class="about">Номер телефона:</div>
                                <input name="phone" placeholder="+38 (097) 787-85-96" value="{{ old('phone') }}" required autofocus type="text">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-block">
                        <div class="form-left">
                            <div class="circle">4</div>
                            <div class="gray-vertical">
                                <div class="vertical"></div>
                            </div>
                        </div>
                        <div class="form-right">
                            <div class="form-subtitle">Аккаунт</div>
                            <div class="block">
                                <div class="about">Почта(Ваш логин):</div>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                                <div class="about">Пароль:</div>
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Пароль не короче 6 символов">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <div class="about">Повтор пароля:</div>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Введите пароль еще раз" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-block">
                        <div class="form-left">
                            <div class="circle">5</div>
                        </div>
                        <div class="form-right">
                            <input class="registration-btn" type="submit" value="Зарегистрироваться">
                        </div>
                    </div>

                </form>
            </div><!---main-->
            <div class="secondary">
                <div class="free-block">
                    <div class="title">
                        <h3>ПУБЛИКАЦИЯ ПРОЕКТА АБСОЛЮТНО БЕСПЛАТНА</h3>
                    </div>
                    <div class="block">
                        <div class="try-now">Попробуйте это сегодня!</div>
                        <ul class="benefit-list">
                            <li>Получайте предложения от опытных мастеров за считаные минуты.</li>
                            <li>Просмотрите профили и рейтинги мастеров, а затем пообщайтесь с ними с помощью чата</li>
                            <li>Выберите исполнителя и отдайте заказ на выполнение.</li>
                            <li>Получите желаемый результат и обменяйтесь отзывами.</li>
                        </ul>
                    </div>
                </div>
            </div><!---secondary-->
        </div>
    </section>


                        {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                            {{--{{ csrf_field() }}--}}

                            {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                                {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

                                    {{--@if ($errors->has('name'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">--}}
                                {{--<label for="name" class="col-md-4 control-label">Surname</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" required autofocus>--}}

                                    {{--@if ($errors->has('surname'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('surname') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('work_category') ? ' has-error' : '' }}">--}}
                                {{--<label for="work_category" class="col-md-4 control-label">work_category</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="work_category" type="text" class="form-control" name="work_category" value="{{ old('work_category') }}" required autofocus>--}}

                                    {{--@if ($errors->has('work_category'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('work_categorys') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">--}}
                                {{--<label for="age" class="col-md-4 control-label">age</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="age" type="text" class="form-control" name="age" value="{{ old('age') }}" required autofocus>--}}

                                    {{--@if ($errors->has('age'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('age') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

                                    {{--@if ($errors->has('email'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                                {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                    {{--@if ($errors->has('password'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-6 col-md-offset-4">--}}
                                    {{--<button type="submit" class="btn btn-primary">--}}
                                        {{--Register--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}

@endsection



