@extends('includes.header')

@section('content')
    <section id="content">
        <div class="wr clear">
            <div class="main">
                <div class="title">
                    <h3>Восстановление пароля</h3>
                </div>
                <div class="restoring-password">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal registration" role="form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="about">Почта(Ваш логин):</div>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                        <div class="about">Пароль:</div>
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Пароль не короче 6 символов">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        <div class="about">Повтор пароля:</div>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Введите пароль еще раз" required>


                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif

                        <input type="submit" value="Восстановить пароль" class="send-btn">

                    </form>

                </div>
            </div><!---main-->
            <div class="secondary">
                <div class="free-block">
                    <div class="title">
                        <h3>ПУБЛИКАЦИЯ ПРОЕКТА АБСОЛЮТНО БЕСПЛАТНА</h3>
                    </div>
                    <div class="block">
                        <div class="try-now">Попробуйте это сегодня!</div>
                        <ul class="benefit-list">
                            <li>Получайте предложения от опытных мастеров за считаные минуты.</li>
                            <li>Просмотрите профили и рейтинги мастеров, а затем пообщайтесь с ними с помощью чата</li>
                            <li>Выберите исполнителя и отдайте заказ на выполнение.</li>
                            <li>Получите желаемый результат и обменяйтесь отзывами.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    

@endsection
