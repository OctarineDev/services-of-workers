@extends('includes.header')

@section('content')
    <section id="content">
        <div class="wr clear">
            <div class="main">
                <div class="title">
                    <h3>Восстановление пароля</h3>
                </div>
                <div class="restoring-password">
                    <div class="ask">Пожалуйста, укажите e-mail, который Вы использовали для входа на сайт. Вы получите письмо с инструкцией о том, как изменить пароль. </div>
                    <div class="email">Ваш e-mail:</div>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form lass="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">

                        {{ csrf_field() }}

                        <input id="email" type="email" class="form-control input-form" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <input type="submit" value="Восстановить пароль" class="send-btn">
                    </form>
                </div>
            </div><!---main-->
            <div class="secondary">
                <div class="free-block">
                    <div class="title">
                        <h3>ПУБЛИКАЦИЯ ПРОЕКТА АБСОЛЮТНО БЕСПЛАТНА</h3>
                    </div>
                    <div class="block">
                        <div class="try-now">Попробуйте это сегодня!</div>
                        <ul class="benefit-list">
                            <li>Получайте предложения от опытных мастеров за считаные минуты.</li>
                            <li>Просмотрите профили и рейтинги мастеров, а затем пообщайтесь с ними с помощью чата</li>
                            <li>Выберите исполнителя и отдайте заказ на выполнение.</li>
                            <li>Получите желаемый результат и обменяйтесь отзывами.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
