@extends('includes.header')
                                                                                
@section('content')
   <section id="content">
      <div class="wr clear">
         <div class="main">
            <div class="title">
               <h3>Подтверждение аккаунта</h3>
            </div>
            <div class="restoring-password">
               <div class="ask"> {{ Auth::user()->name }}, на ваш email {{ Auth::user()->email }} отправлено письмо с инструкцией. Проследуйте ей, чтобы завершить регистрацию. </div>
            </div>
         </div><!---main-->
         <div class="secondary">
            <div class="free-block">
               <div class="title">
                  <h3>ПУБЛИКАЦИЯ ПРОЕКТА АБСОЛЮТНО БЕСПЛАТНА</h3>
               </div>
               <div class="block">
                  <div class="try-now">Попробуйте это сегодня!</div>
                  <ul class="benefit-list">
                     <li>Получайте предложения от опытных мастеров за считаные минуты.</li>
                     <li>Просмотрите профили и рейтинги мастеров, а затем пообщайтесь с ними с помощью чата</li>
                     <li>Выберите исполнителя и отдайте заказ на выполнение.</li>
                     <li>Получите желаемый результат и обменяйтесь отзывами.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </section>
@endsection
