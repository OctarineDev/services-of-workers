@extends('includes.header')

@section('content')
    <section id="content">
        <div class="wr clear">
            <div class="main">
                <div class="title">
                    <h3>Вход</h3>
                </div>
                <form class="form-horizontal registration" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-block">
                        <div class="form-left">
                            <div class="circle"></div>
                            <div class="gray-vertical">
                                <div class="vertical"></div>
                            </div>
                        </div>
                        <div class="form-right">
                            <div class="form-subtitle">Аккаунт</div>
                            <div class="block">
                                <div class="about">Почта(Ваш логин):</div>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                                <div class="about">Пароль:</div>
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Пароль не короче 6 символов">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-block">
                            <div class="form-right">
                                <input class="registration-btn" type="submit" value="Войти">
                            </div>
                        </div>                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Запомнить меня
                        </label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <a class="btn btn-link" href="{{ route('register') }}">
                                Зарегистрироваться
                            </a>

                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Забыли пароль?
                            </a>
                        </div>
                    </div>
                </form>
            </div><!---main-->
            <div class="secondary">
                <div class="free-block">
                    <div class="title">
                        <h3>ПУБЛИКАЦИЯ ПРОЕКТА АБСОЛЮТНО БЕСПЛАТНА</h3>
                    </div>
                    <div class="block">
                        <div class="try-now">Попробуйте это сегодня!</div>
                        <ul class="benefit-list">
                            <li>Получайте предложения от опытных мастеров за считаные минуты.</li>
                            <li>Просмотрите профили и рейтинги мастеров, а затем пообщайтесь с ними с помощью чата</li>
                            <li>Выберите исполнителя и отдайте заказ на выполнение.</li>
                            <li>Получите желаемый результат и обменяйтесь отзывами.</li>
                        </ul>
                    </div>
                </div>
            </div><!---secondary-->
        </div>
    </section>

@endsection
