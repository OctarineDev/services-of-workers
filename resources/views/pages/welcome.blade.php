@extends('includes.header')

@section('content')
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
    <div class="top-right links">
        @if (Auth::check())
        <a href="{{ url('/') }}">Home</a>
        @else
        <a href="{{ url('/login') }}">Login</a>
        <a href="{{ url('/register') }}">Register</a>
        @endif
    </div>
    @endif
    @foreach ($users as $user)
    <p>{{ $user->name}}</p>
    @if($user->isOnline())
    user is online!!
    @else
    {{ $user->last_visited }}
    @endif
    @endforeach
    <div class="content">
        <table border="1">
            <caption>ПРоекты</caption>
            <tr>
                <th>Название проекта</th>
                <th>Категоиря</th>
                <th>Контент</th>
                <th>Прик. файлы</th>
            </tr>
            @foreach ($projects as $project)
            <tr>
                <td>{{ $project->name }}</td>
                <td>{{ $project->category }}</td>
                <td>{!! $project->text !!}</td>
                @if($project->file != '')
                @foreach ($project->file as $file)
                <td><a  href="{{ asset('download/'. $project->directory .'/'. $file) }}" target="_blank" >{{ $file }}</a></td>
                @endforeach
                @endif
            </tr>
            @endforeach

        </table>
    </div>


</div>
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form action="{{ route('add.project') }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <p><label>Название проекта</label><input type="" name="name" value="{{ old('name') }}"></p>

    <p><label>Категория</label><input type="" name="category" value="{{ old('category') }}"></p>

    <p><label>Описание</label><textarea type="" name="text" value="{{ old('text') }}"></textarea></p>

    <input type="file" name="file[]" multiple id="files" value="" validate="true">

    <div id="selectedFiles"></div>


    <input type="submit" value="Добавить проект">


</form>

<script src="{{ URL::to('js/tinymce/tinymce.min.js') }}"></script>
<script>
    document.addEventListener("DOMContentLoaded", init, false);

    function init() {
        document.querySelector('#files').addEventListener('change', handleFileSelect, false);
        selDiv = document.querySelector("#selectedFiles");
    }

    function handleFileSelect(e) {

        if (!e.target.files) return;


        var files = e.target.files;
        for (var i = 0; i < files.length; i++) {
            var f = files[i];

            selDiv.innerHTML += f.name + "<br/>";

        }

    }

    var editor_config = {
        path_absolute: "{{ URL::to('/') }}/",
        selector: "textarea",
        plugins: [
            "advlist autolink lists link  charmap  preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime  nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        menu: {
        },
        toolbar: " undo redo | bold italic underline  | alignleft aligncenter alignright alignjustify | bullist numlist | link ",
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementByTagName('body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document.grtElementByTagName('body')[0].clientHeight;
            var cmsURL = editor_config.path_absolute + 'laravel-filemanaget?field_name' + field_name;
            if (type = 'image') {
                cmsURL = cmsURL + '&type=Images';
            } else {
                cmsUrl = cmsURL + '&type=Files';
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizeble: 'yes',
                close_previous: 'no'
            });
        }
    };

    tinymce.init(editor_config);
</script>

@endsection
