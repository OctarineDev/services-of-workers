<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Биржа Мастеров</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.fs.selecter.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<header class="header-top registered-user">
    <ul class="drop-down-menu">
        <li><a href="#">Главная</a></li>
        <li><a href="#">Мастера</a></li>
        <li><a href="#">Заказы </a></li>
        <li><a href="#">Новости</a></li>
        <li><a href="#">Помощь </a></li>
        <li><a href="#">Рассчитать стоимость ремонта</a></li>
    </ul>
    <div class="wr">
        <div class="left-block">
            <div class="drop-down-toggle">
                <div class="toggle-line"></div>
                <div class="toggle-line"></div>
                <div class="toggle-line"></div>
            </div>
            <div class="block">
                <a href="{{ url('/') }}" class="logo"><img src="{{ url('img/logo.png') }}" alt="logo"></a>
                <ul class="header-menu">
                    <li><a href="#">Мастера</a></li>
                    <li><a href="#">Заказы </a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Помощь </a></li>
                </ul>
            </div>
        </div>
        <div class="right-block">

            @if (Auth::guest())
                <li><a class="registration-a" href="{{ route('register') }}">Регистрация</a></li>
                <li><a class="enter-a" href="{{ route('login') }}" >Войти</a></li>
            @else
            <div class="header-user">
                <a href="#"><span class="message"></span></a>
                <a href="#">
                    <img class="user-icon" src="{{ url('img/header-user-photo.png') }}" alt="user">
                    <div class="user-name">{{ Auth::user()->name }}</div>
                </a>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <ul class="drop-menu">
                                <li><a href="#">Редактировать профиль</a></li>
                                <li><a href="#">пункт меню</a></li>
                                <li><a href="{{ route('password.request')  }}">Восстановить пароль</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Выйти
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </div>
            @endif
        </div>
    </div>
    
    <!-------------------pop up------------------------>
       
    <section class="pop-up">
        <div class="window">
            <div class="top">
                <div class="block">
                    <div class="proj-name">Название проекта</div>
                    <a href="#" class="edit-btn">Редактировать</a>
                </div>
                
                <div class="exit"></div>
            </div>
            <p>Я занимаюсь профессиональным ремонтом чего либо, после ремонта предоставляю качественную уборку терриотрии. Дополнительно вы можете получить от меня рекомендации. Так же можем разрабатывать планы , берем отвернултственность оформления документов на себя (Здесь будет текст объясняющий, чем занимается человек, чтобы не выводить категории).</p>
            <img src="img/popup.png" alt="img">
            <img src="img/popup.png" alt="img">
        </div>
    </section>
    
    <section class="pop-up-reg">
        <div class="window">
            <div class="top">
                <p>Вход</p>
                <div class="exit"></div>
            </div>
            <form action="">
                <div class="about">Email или телефон</div>
                <input type="text">
                <div class="about">Пароль</div>
                <input type="text">
                <input class="enter-btn" type="submit" value="Войти">
            </form>
            <a href="#">Забыли пароль?</a>
            <a href="#">Зарегистрироваться</a>
        </div>
    </section>
    
    <!------------------------------------------------->

</header>
@yield('content')

@include('includes.footer')


