<footer>
    <div class="wr">
        <div class="footer-top">
            <div class="footer-link">
                <ul>
                    <li class="footer-nav"><a href="#">Правила сервиса</a></li>
                    <li class="footer-nav"><a href="#">Тарифы</a></li>
                    <li class="footer-nav"><a href="#">Служба поддержки</a></li>
                </ul>
            </div>
            <div class="footer-social">
                <a href="#" class="circle"> <i class="fa fa-vk" aria-hidden="true"></i> </a>
                <a href="#" class="circle"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                <a href="#" class="circle"> <i class="fa fa-instagram" aria-hidden="true"></i> </a>
                <a href="#" class="circle"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
            </div>
        </div>
        <div class="footer-bot">
            <div class="footer-copy">© 2017 Биржа мастеров</div>
            <a href="#" class="footer-developed">Разработано в Octarine</a>
        </div>
    </div>
</footer>
<link href="{{ asset('css/jquery.fs.selecter.css') }}" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/jquery.fs.selecter.min.js') }}"></script>
<script src="{{ asset('js/modernizr.js') }}"></script>
</body>
</html>