<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть не меньше 6 символов',
    'reset' => 'Ваш пароль успешно обновлён',
    'sent' => 'На Вашу почту отправлено письмо для восстанвления пароля!',
    'token' => 'This password reset token is invalid.',
    'user' => "Пользователя с такой почтой не существует",

];
