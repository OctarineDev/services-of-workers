<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('role');
            $table->string('birth_day');
            $table->string('birth_month');
            $table->string('birth_year');
            $table->string('gender');
            $table->string('city');
            $table->string('phone');
            $table->string('key');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('confirmed')->default(false);
            $table->timestamp('last_visited')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
