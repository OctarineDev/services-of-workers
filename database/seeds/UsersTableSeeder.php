<?php

use Illuminate\Database\Seeder;
use App\User as User;


class UsersTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create( [
            'email' => 'rozhden.t@gmail.com' ,
            'password' => Hash::make( '41p1dros' ) ,
            'name' => 'Радатан' ,
            'surname' => 'Тоцкойнов' ,
            'work_category' => 'продаю бананы' ,
            'age' => '22' ,
        ] );

        User::create( [
            'email' => '1sakhno.tolik@gmail.com' ,
            'password' => Hash::make( '1488tolik' ) ,
            'name' => 'Лысый' ,
            'surname' => 'Сахно' ,
            'work_category' => 'собираю бананы' ,
            'age' => '21' ,
        ] );
    }
}
