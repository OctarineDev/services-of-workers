<?php

namespace Horsefly\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Horsefly\Events\UserWasRegister;

class Notification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event)
    {
        $this->userMail = $event->userMail;
        $this->key = $event->key;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $userMail = encrypt($this->userMail);
        $key = $this->key;
        return $this->view('notifications.reg_mail')->with(compact(['userMail','key']));
    }
}
