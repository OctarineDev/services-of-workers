<?php

namespace Horsefly;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $casts = [
        'file' => 'array',
    ];
}
