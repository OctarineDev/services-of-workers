<?php

namespace Horsefly\Listeners\Account;

use Horsefly\Events\UserWasRegister;
use Illuminate\Queue\InteractsWithQueue;
use Horsefly\Mail\Notification;

class SendNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasRegister  $event
     * @return void
     */
    public function handle(UserWasRegister $event)
    {
        \Mail::to($event->userMail)->send(new Notification($event));
    }
}
