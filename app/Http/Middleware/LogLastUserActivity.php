<?php

namespace Horsefly\Http\Middleware;

use Closure;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Horsefly\User;
use Laravelrus\LocalizedCarbon\LocalizedCarbon;


class LogLastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            Carbon::setLocale('ru');
            $expiresAt = Carbon::now('Europe/Kiev')->addMinutes(1);
            Cache::put('user-is-online-' . Auth::user()->id, $expiresAt, $expiresAt);
            $user = Auth::user();
            $user->last_visited = Carbon::now('Europe/Kiev');
            $user->save();

           // dd(Carbon::now('Europe/Moscow'));
          // dd(Carbon::now('Europe/Moscow')->parse(Cache::get('user-is-online-1'))->diffForHumans());
        }
        return $next($request);
    }
}
