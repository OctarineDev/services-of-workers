<?php
namespace Horsefly\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
class isVerification {
/**
* Handle an incoming request.
*
* @param  \Illuminate\Http\Request  $request
* @param  \Closure  $next
* @param  string|null  $guard
* @return mixed
*/
  public function handle($request, Closure $next, $guard = null) {
    if (Auth::check()) {
      if (Auth::user()->confirmed == 0) {
        return response()->view('auth.verification_message');
      }
      else {
        return $next($request);
      }
    }
    else {
      return redirect('/login');
    }
  }
}