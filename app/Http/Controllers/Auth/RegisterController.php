<?php

namespace Horsefly\Http\Controllers\Auth;

use Horsefly\Events\UserWasRegister;
use Horsefly\User;
use Horsefly\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:15',
            'surname' => 'required|string|max:15',
            'birth_day' => 'required',
            'birth_month' => 'required',
            'birth_year' => 'required',
            'city' => 'required',
            'phone' => 'required|max:30',

            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $userMail = $data['email'];
        $key = str_random(60);
        event(new UserWasRegister($userMail,$key));
        return User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'birth_day' => $data['birth_day'],
            'birth_month' => $data['birth_month'],
            'birth_year' => $data['birth_year'],
            'gender' => $data['gender'],
            'city' => $data['city'],
            'phone' => $data['phone'],
            'role' => $data['role'],
            'key' => $key,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
