<?php

namespace Horsefly\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;

use Horsefly\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Horsefly\User;

class AccountVerificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->date = date('Y-m-d');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($key)
    {
        $key_db = Auth::user()->key;
        if ($key_db == $key):
            $user = User::where('key', $key)->update(['confirmed' => 1]);
            return view('pages.home');
        endif;

    }
}