<?php

namespace Horsefly\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;
use Horsefly\Project;
use Horsefly\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->date = date('Y-m-d');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $projects = Project::all();
        $users = User::all();
        foreach ($projects as $project) {
            if ($project->file != '') {
                $files = (json_decode($project->file));
                $project->file = $files;
            }
        }

        foreach($users as $user){
            $last_visited = $user->last_visited;
            Carbon::setLocale('ru');
            $user->last_visited = Carbon::parse($last_visited)->diffForHumans();
        }

        return view('pages.welcome')->with(compact(['projects','users']));
    }

    public function welcome()
    {
        return view('pages.home');

    }

    public function add()
    {
        Input::flash();

        $rules = [
            'name' => 'required|unique:projects|max:255',
            'category' => 'required',
            'text' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }


        $project = new Project();

        $name = Input::get('name');
        $category = Input::get('category');
        $text = Input::get('text');

        $project->name = $name;
        $project->category = $category;
        $project->text  = $text;
        $project->directory  = $this->date;

        if (Input::file('file') != NULL) {

            $files = array();

            foreach (Input::file("file") as $file) {
                $random_number_first = rand(0, 9);
                $random_number_second = rand(0, 9);
                $random_number_third = rand(0, 9);
                $random_number_fourth = rand(0, 9);

                $fileName = $file->getClientOriginalName();

                $fileName = $random_number_first . $random_number_second . $random_number_third . $random_number_fourth . '_' . $fileName;

                $file->storeAs($this->date,$fileName);

                array_push($files, $fileName);
            }
            $files = json_encode($files);
            $project->file = $files;
        }

        $project->save();

        return Redirect::back()->withErrors($validator);
    }

    public function download($date,$fileName)
    {

        $path = storage_path('app/files/'.$date.'/'.$fileName);
        return response()->download($path);
    }


}
