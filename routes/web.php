<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');


/*
* Account routes
*/
Route::get('/account/activation/code={key}', ['as' => 'AccountVerification', 'uses' => 'Auth\AccountVerificationController@index']);
Route::get('/home', 'HomeController@index');


Route::group(['middleware' => ['is.verification']], function () {
    
    Route::get('/home', 'HomeController@index');

    Route::post('/addProject', ['as' => 'add.project', 'uses' => 'HomeController@add']);
    Route::get('/download/{date}/{fileName}', ['as' => 'download', 'uses' => 'HomeController@download']);

});


Auth::routes();
