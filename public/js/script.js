$(document).ready(function() {
    
   $('.look-portfolio').on('click', function(){
       $(this).parent().siblings('.mini-portfolio').fadeIn(300).addClass('active-mini'); 
       $(this).siblings('.hide-mini-portfolio').fadeIn(0);
       $(this).fadeOut(0);
       
   }); 
    $('.hide-mini-portfolio').on('click', function(){
        $(this).parent().siblings('.mini-portfolio').fadeOut(300).removeClass('active-mini'); 
        $(this).fadeOut(0);
        $(this).siblings('.look-portfolio').fadeIn(0);
   });
    
    if( $(window).width()<1199 ){ 
        $('.order').each(function(index){
            $(this).find('.bid-amount').appendTo($(this).find('.order-bot'));
            $(this).find('.price').appendTo($(this).find('.order-mid .block'));
        });
        
        if( $(window).width()<480 ){ 
        $('.order').each(function(index){
            $(this).find('.order-mid').after($(this).find('.price'));
        });
    }
    }
    
    if( $(window).width()<769 ){
        $('.main').prepend($('.advertisement'));
        $('.main>.swap-block .paragraph-block').after($('.advertisement'));
        $('.main').after($('.we-in-social'));
        $('.main').after($('.serv-stat'));
        $('.categories-mobile .drop-down').append($('.categories'));
        $('.filter-mobile .drop-down').append($('.filter'));
        
        if( $(window).width()<480 ){
            $('.card').each(function(index){
                $(this).find('.right-block').after($(this).find('.no-ad-bottom'));
                $(this).find('.no-ad-bottom').after($(this).find('.hire'));//card buttons replace
                $(this).find('.no-ad-bottom').after($(this).find('.chat'));//card buttons replace
            }); 
        }
        
        $('.top-blue-line .improve-block').append($('.top-blue-line .status'));
        $('.order').each(function(index){
            $(this).find('.rang').before($(this).find('.status'));
        });
        $('.last-rewiev .cart-bottom-top').append($('.last-rewiev .bid'));
        $('.last-rewiev .cart-bottom-top').append($('.last-rewiev .how-long'));
    }
    
    $('.categories-mobile>.block, .filter-mobile>.block').on('click', function(){ 
        $(this).siblings('.drop-down').toggle();
    });
    
    $('header .drop-down-toggle').on('click', function(){ 
        $('.drop-down-menu').toggle();
    });//mobile menu toggle
    
    $('.cat-menu li').on('click', function(){ 
        if( $(this).hasClass('active-li') ){  
            $(this).removeClass('active-li');
        }
        else{
            $('.cat-menu li').removeClass('active-li');
            $(this).addClass('active-li');
        }     
    }); //catalog-menu animation
    
    $(document).ready(function(){
        $("#select").selecter();
        $("#select1").selecter();
        $("#select2").selecter();
        $("#select3").selecter();
        $("#select4").selecter();
        $("#select5").selecter();
        $("#select6").selecter();
        $("#select7").selecter();
        $("#select8").selecter();
        $("#select9").selecter();
        $("#select10").selecter();
        $("#select11").selecter();
        $("#select12").selecter();
        
    });// customize select
    
     $('body').on('change', '.selecter-element', function(){
        $(this).siblings('.selecter-selected').css('border-color', '#289dca');
    });
    
    $('input[type=text]').on('blur', function(){
        if($(this).val().length>0)
            $(this).css('border-color','#289dca'); 
        else
            $(this).css('border-color','#c2c2c2');      
    });

    var fBlock = $('.form-block'),
        len = fBlock.length,
        i = 0,
        delay = 500,
        intervalId = setInterval(function () {
            if (i < len) {
                fBlock.eq(i).find('.circle').addClass('circle-blue');
                fBlock.eq(i).find('.form-right').addClass('form-right-visible');
                i += 1;
            } else {
                clearInterval(intervalId);
            }
        }, delay);
    
    var fBlock2 = $('.form-block'),
        len2 = fBlock2.length,
        i2 = 0,
        delay2 = 600,
        intervalId2 = setInterval(function () {
            if (i2 < len2) {
                fBlock2.eq(i2).find('.vertical').addClass('vertical-100-height');
                i2 += 1;
            } else {
                clearInterval(intervalId2);
            }
        }, delay2);
    
    $('.load-button').on('click', function(){
        $(this).siblings('.load-input').trigger('click');
    });
    
    //skript for one_master_page | |
    //                           v v
    
    if( $(window).width()<992 ){
        $('.mob-contact').append($('#master .secondary .contact'));
        $('.mob-skills').append($('#master .secondary .skills'));
        $('.mob-rating-block').append($('#master .secondary .rating-block'));
    }
    
    $('.pop-up .exit').on('click', function(){
        $('.pop-up').fadeOut(200); 
    });
    $('.pop-up-reg .exit').on('click', function(){
        $('.pop-up-reg').fadeOut(200); 
    });
    
    $('.enter-a').on('click', function(){
        $('.pop-up-reg').fadeIn(200).css('display','flex'); 
    });
    
    $('#master-profile .portfolio-block img').on('click', function(){
        $('.pop-up').fadeIn(200).css('display','flex'); 
    });
    
    $('.mob-contact').on('click', function(){ 
        $('.contact').toggle();
    });
    
    $('.mob-skills').on('click', function(){ 
        $('.skills').toggle();
    });
    
    $('.mob-rating-block').on('click', function(){ 
        $('.rating-block').toggle();
    });
    
    if( $(window).width()<764 ){
         $('#master .master-info .wrap-block .ovr').before($('#master .master-info-top'));
    }
    
    if( $(window).width()<480 ){
        $('#master .card').each(function(index){
            $(this).find('.mob-top-block').append($(this).find('.location'));
            $(this).find('.mob-top-block').append($(this).find('.how-long'));
         });
    }
    
    $('.user-name').on('click', function(){
        if( !($('.dropdown').hasClass('active')) ) 
            $('.dropdown').fadeIn(200).addClass('active');
        else
            $('.dropdown').fadeOut(200).removeClass('active');
        
    });
         
});